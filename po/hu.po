# Hungarian translation for tempo.
# Copyright (C) 2013 tempo's COPYRIGHT HOLDER
# This file is distributed under the same license as the tempo package.
#
# Balázs Úr <urbalazs@gmail.com>, 2013.
# Balázs Úr <urbalazs at gmail dot com>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: tempo master\n"
"Report-Msgid-Bugs-To: http://bugzilla.gnome.org/enter_bug.cgi?"
"product=tempo&keywords=I18N+L10N&component=general\n"
"POT-Creation-Date: 2013-03-04 09:52+0000\n"
"PO-Revision-Date: 2013-03-16 20:28+0100\n"
"Last-Translator: Balázs Úr <urbalazs at gmail dot com>\n"
"Language-Team: Hungarian <gnome-hu-list@gnome.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 1.2\n"

#: ../data/tempo.desktop.in.in.h:1
msgid "Weather"
msgstr "Időjárás"

#: ../data/tempo.desktop.in.in.h:2
msgid "Check the weather"
msgstr "Időjárás ellenőrzése"

#: ../tempo/mainWindow.js:256
msgid "Edit location"
msgstr "Hely szerkesztése"

#: ../tempo/org.gnome.tempo.gschema.xml.in.h:1
msgid "Last location"
msgstr "Legutóbbi hely"

#: ../tempo/org.gnome.tempo.gschema.xml.in.h:2
msgid "Last location typed by the user."
msgstr "A felhasználó által begépelt legutóbbi hely."

#: ../tempo/org.gnome.tempo.gschema.xml.in.h:3
msgid "Refresh interval"
msgstr "Frissítési időköz"

#: ../tempo/org.gnome.tempo.gschema.xml.in.h:4
msgid "Refresh interval, in seconds."
msgstr "Frissítési időköz másodpercben."

#: ../tempo/org.gnome.tempo.gschema.xml.in.h:5
msgid "Window position"
msgstr "Ablakpozíció"

#: ../tempo/org.gnome.tempo.gschema.xml.in.h:6
msgid "Window position (x and y)."
msgstr "Ablakpozíció (x és y)."

