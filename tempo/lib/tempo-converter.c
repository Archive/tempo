/*
 * Copyright (c) 2011 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "tempo-converter.h"

static gchar *
convert_from_charset (const gchar *data,
                      goffset data_len,
                      const gchar *charset)
{
  GCharsetConverter *converter;
  GInputStream *filter, *base;
  GError *error = NULL;
  gchar buffer[256];
  GString *str;
  gssize bytes_read;
  gchar *retval = NULL;

  converter = g_charset_converter_new ("UTF-8", charset,
                                       &error);

  if (error != NULL) {
    g_warning ("Cannot convert from %s to UTF-8: %s\n",
               charset, error->message);
    g_error_free (error);

    return NULL;
  }

  base = g_memory_input_stream_new_from_data (data, data_len, NULL);
  filter = g_converter_input_stream_new (base, G_CONVERTER (converter));

  str = g_string_new (NULL);
  bytes_read = 0;

  do {
    bytes_read = g_input_stream_read (filter,
                                      &buffer, 256,
                                      NULL, &error);

    if (error != NULL) {
      g_warning ("Cannot convert from %s to UTF-8: %s\n",
                 charset, error->message);
      g_error_free (error);
      g_string_free (str, TRUE);
      goto out;
    }

    g_string_append_len (str, buffer, bytes_read);
  } while (bytes_read > 0);

  retval = g_string_free (str, FALSE);

 out:
  g_object_unref (base);
  g_object_unref (filter);
  g_object_unref (converter);

  return retval;
}

gchar *
tempo_convert_response_to_utf8 (SoupMessage *message)
{
  SoupMessageBody *body = message->response_body;
  SoupMessageHeaders *headers = message->response_headers;
  GHashTable *params;
  const gchar *charset;
  gchar *retval;

  soup_message_headers_get_content_type (headers, &params);
  charset = g_hash_table_lookup (params, "charset");

  retval = convert_from_charset (body->data, body->length, charset); 

  return retval;
}
