/*
 * Copyright (c) 2011 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

#include "tempo-utils.h"

#include <gtk/gtk.h>

static void
_cairo_round_rectangle (cairo_t *cr,
			gdouble	  x,
			gdouble	  y,
			gdouble	  w,
			gdouble	  h,
			gdouble	  radius)
{
  g_return_if_fail (cr != NULL);

  if (radius < 0.0001) {
    cairo_rectangle (cr, x, y, w, h);
    return;
  }

  cairo_move_to (cr, x+radius, y);
  cairo_arc (cr, x+w-radius, y+radius, radius, G_PI * 1.5, G_PI * 2);
  cairo_arc (cr, x+w-radius, y+h-radius, radius, 0, G_PI * 0.5);
  cairo_arc (cr, x+radius,   y+h-radius, radius, G_PI * 0.5, G_PI);
  cairo_arc (cr, x+radius,   y+radius,   radius, G_PI, G_PI * 1.5);
}

static void
window_size_allocate_cb (GtkWidget *widget,
                         GtkAllocation *allocation,
                         gpointer user_data)
{
  cairo_surface_t *surface;
  cairo_t *cr;
  cairo_region_t *shape;

  surface = cairo_image_surface_create (CAIRO_FORMAT_A8,
                                        allocation->width,
                                        allocation->height);
  cr = cairo_create (surface);

  cairo_save (cr);
  cairo_rectangle (cr, 0, 0, allocation->width, allocation->height);
  cairo_set_operator (cr, CAIRO_OPERATOR_CLEAR);
  cairo_fill (cr);
  cairo_restore (cr);

  cairo_set_source_rgb (cr, 1, 1, 1);
  _cairo_round_rectangle (cr, 0, 0, allocation->width, allocation->height, 5);
  cairo_fill (cr);

  cairo_destroy (cr);

  shape = gdk_cairo_region_create_from_surface (surface);
  cairo_surface_destroy (surface);

  gtk_widget_shape_combine_region (widget, shape);
  cairo_region_destroy (shape);
}

/**
 * tempo_ensure_rounded_corners:
 * @window: a #GtkWindow
 *
 */
void
tempo_ensure_rounded_corners (GtkWindow *window)
{
  g_signal_connect (window, "size-allocate",
                    G_CALLBACK (window_size_allocate_cb), NULL);
}
