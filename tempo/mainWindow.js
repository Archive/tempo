/*
 * Copyright (c) 2011 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

const ForecastBox = imports.forecastBox;
const Main = imports.main;

const Gdk = imports.gi.Gdk;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Path = imports.path;
const Tempo = imports.gi.Tempo;
const _ = imports.gettext.gettext;

const _CONFIGURE_ID_TIMEOUT = 100; // msecs

function MainWindow() {
    this._init();
}

MainWindow.prototype = {
    _init: function() {
        this._configureId = 0;

        this._gtkWindow = new Gtk.Window({ type: Gtk.WindowType.TOPLEVEL,
                                           decorated: false,
                                           has_resize_grip: false,
                                           resizable: false,
                                           window_position: Gtk.WindowPosition.CENTER });
        Tempo.ensure_rounded_corners(this._gtkWindow);

        this._gtkWindow.connect('delete-event',
                                Lang.bind(this, this._quitApp));
        this._gtkWindow.connect('key-press-event',
                                Lang.bind(this, this._onKeyPressEvent));
        this._gtkWindow.connect('configure-event',
                                Lang.bind(this, this._onConfigureEvent));

        // build an event box to trap clicks on the window
        this._eventBox = new Gtk.EventBox();
        this._gtkWindow.add(this._eventBox);
        this._eventBox.connect('button-press-event',
                               Lang.bind(this, this._onButtonPressEvent));

        // build the UI
        this._mainGrid = new Gtk.Grid({ orientation: Gtk.Orientation.VERTICAL,
                                        row_spacing: 12,
                                        margin_left: 12,
                                        margin_right: 12,
                                        margin_top: 6,
                                        margin_bottom: 6 });
        this._eventBox.add(this._mainGrid);

        // decoGrid holds the pseudo window decorations
        this._decoGrid = new Gtk.Grid({ orientation: Gtk.Orientation.HORIZONTAL });
        this._mainGrid.add(this._decoGrid);

        this._cityLabel = new Gtk.Label({ halign: Gtk.Align.START,
                                          hexpand: true,
                                          name: 'tempo-city-label' });
        this._decoGrid.add(this._cityLabel);

        this._settingsButton =
            new Gtk.Button({ child: new Gtk.Image ({ icon_size: Gtk.IconSize.MENU,
                                                     icon_name: 'system-run-symbolic',
                                                     halign: Gtk.Align.END }),
                             relief: Gtk.ReliefStyle.NONE });
        this._settingsButton.connect('clicked',
                                     Lang.bind(this, this._showSettings));
        this._decoGrid.add(this._settingsButton);

        this._quitButton =
            new Gtk.Button({ image: new Gtk.Image ({ icon_size: Gtk.IconSize.MENU,
                                                     icon_name: 'window-close-symbolic',
                                                     halign: Gtk.Align.END }),
                             relief: Gtk.ReliefStyle.NONE });
        this._quitButton.connect('clicked',
                                 Lang.bind(this, this._quitApp));
        this._decoGrid.add(this._quitButton);

        // boxGrid holds the primary information
        this._boxGrid = new Gtk.Grid({ orientation: Gtk.Orientation.HORIZONTAL,
                                       column_spacing: 16 });
        this._mainGrid.add(this._boxGrid);
        this._icon = new Gtk.Image({ pixel_size: 48,
                                     halign: Gtk.Align.START,
                                     margin_left: 12 });
        this._boxGrid.add(this._icon);

        this._degreeLabel = new Gtk.Label({ halign: Gtk.Align.START,
                                            name: 'tempo-degree-label' });
        this._boxGrid.add(this._degreeLabel);

        // infoGrid holds the right side of the primary information
        this._infoGrid = new Gtk.Grid({ orientation: Gtk.Orientation.VERTICAL,
                                        row_spacing: 3 });
        this._boxGrid.add(this._infoGrid);

        this._condLabel = new Gtk.Label({ halign: Gtk.Align.START,
                                          name: 'tempo-condition-label' });
        this._infoGrid.add(this._condLabel);

        this._humidityLabel = new Gtk.Label({ halign: Gtk.Align.START,
                                              margin_left: 1,
                                              name: 'tempo-detail-label' });
        this._infoGrid.add(this._humidityLabel);

        this._windLabel = new Gtk.Label({ halign: Gtk.Align.START,
                                          margin_left: 1,
                                          name: 'tempo-detail-label' });
        this._infoGrid.add(this._windLabel);

        // separator between the primary and secondary sections
        let separator = new Gtk.Separator({ orientation: Gtk.Orientation.HORIZONTAL,
                                            hexpand: true,
                                            name: 'tempo-separator' });
        this._mainGrid.add(separator);

        // forecastGrid holds the bottom secondary information
        this._forecastGrid = new Gtk.Grid({ orientation: Gtk.Orientation.HORIZONTAL,
                                            hexpand: true,
                                            halign: Gtk.Align.CENTER,
                                            column_spacing: 12,
                                            margin_left: 12,
                                            margin_right: 12 });
        this._mainGrid.add(this._forecastGrid);
        this._createForecastGrid();

        // connect to the loader signals and see if we can populate
        // something from the cache already
        Main.loader.connect('changed', Lang.bind(this, this._onLoaderChanged));
        Main.loader.connect('loading', Lang.bind(this, this._onLoaderLoading));
        Main.loader.connect('error', Lang.bind(this, this._onLoaderError));
        this._onLoaderChanged(Main.loader);

        // show all
        this._gtkWindow.show_all();

        // apply the last saved window position
        let position = Main.settings.get_value('window-position');
        if (position.n_children() == 2) {
            let x = position.get_child_value(0);
            let y = position.get_child_value(1);

            this._gtkWindow.move(x.get_int32(),
                                 y.get_int32());
        }

        if (Main.settings.get_string('last-location') == '') {
            this._showSettings();
            return;
        }
    },

    _quitApp : function() {
        // remove configure event handler if still there
        if (this._configureId != 0) {
            Mainloop.source_remove(this._configureId);
            this._configureId = 0;
        }

        // always save before quitting
        this._saveWindowPosition();

        Gtk.main_quit();
    },

    _saveWindowPosition: function() {
        // GLib.Variant.new() can handle arrays just fine
        let position = this._gtkWindow.get_position();
        let variant = GLib.Variant.new ('ai', position);
        Main.settings.set_value('window-position', variant);
    },

    _onConfigureEvent: function(widget, event) {
        if (this._configureId != 0) {
            Mainloop.source_remove(this._configureId);
            this._configureId = 0;
        }

        this._configureId = Mainloop.timeout_add(_CONFIGURE_ID_TIMEOUT, Lang.bind(this,
            function() {
                this._saveWindowPosition();
                return false;
            }));
    },

    _onKeyPressEvent : function(widget, event) {
        let keyval = event.get_keyval()[1];
        let state = event.get_state()[1];

        // quit with Ctrl+Q
        if ((keyval == Gdk.KEY_q) &&
            ((state & Gdk.ModifierType.CONTROL_MASK) != 0)) {
            this._quitApp();
            return true;
        }

        return false;
    },

    _onButtonPressEvent: function(widget, event) {
        // drag on click
        let rootCoords = event.get_root_coords();
            this._gtkWindow.begin_move_drag(1,
                                            rootCoords[1],
                                            rootCoords[2],
                                            event.get_time());

        return false;
    },

    _onEntryChanged : function() {
        this._okButton.set_sensitive((this._settingsEntry.get_text() != ""));
    },

    _showSettings : function() {
        if (this._settingsDialog) {
            this._settingsDialog.present();
            return;
        }

        this._settingsDialog = new Gtk.Dialog({ resizable: false });
        this._settingsDialog.set_transient_for(this._gtkWindow);
        this._settingsDialog.set_modal(true);
        this._settingsDialog.set_border_width(6);

        this._settingsDialog.add_button('gtk-cancel',
                                        Gtk.ResponseType.CANCEL);
        this._okButton = this._settingsDialog.add_button('gtk-ok',
                                                         Gtk.ResponseType.ACCEPT);
        this._settingsDialog.set_default_response(Gtk.ResponseType.ACCEPT);

        let content = this._settingsDialog.get_content_area();
        let grid = new Gtk.Grid ({ orientation: Gtk.Orientation.HORIZONTAL,
                                   column_spacing: 12});

        let label = new Gtk.Label({ label: _("Edit location"),
                                    margin_left: 6 });
        grid.add(label);

        this._settingsEntry = new Gtk.Entry({ margin_right: 6,
                                              activates_default: true });
        this._settingsEntry.connect('changed',
                                    Lang.bind(this, this._onEntryChanged));

        this._settingsEntry.set_text(Main.settings.get_string('last-location'));
        grid.add(this._settingsEntry);

        content.pack_start(grid, true, true, 6);

        this._settingsDialog.reset_style();

        this._settingsDialog.show_all();
        this._settingsDialog.connect('response',
                                     Lang.bind(this, this._onDialogResponse));
    },

    _onDialogResponse : function(dialog, id) {
        if (id == Gtk.ResponseType.ACCEPT) {
            let newLocation = this._settingsEntry.get_text();

            if (newLocation != Main.settings.get_string('last-location')) {
                Main.settings.set_string('last-location', newLocation);
                Main.loader.setLocation(newLocation);
            }
        }

        this._settingsDialog.destroy();
        delete this._settingsDialog;
    },

    _createForecastGrid : function() {
        let box;
        this._forecastBoxes = new Array();

        for (let i = 0; i < 4; i++) {
            box = new ForecastBox.ForecastBox();
            this._forecastBoxes[i] = box;
            this._forecastGrid.add(box.grid);

            if (i < 3) {
                let separator = new Gtk.Separator({ orientation: Gtk.Orientation.VERTICAL,
                                                    vexpand: true,
                                                    margin_top: 20,
                                                    margin_bottom: 20,
                                                    name: 'tempo-separator' });
                this._forecastGrid.add(separator);
            }
        }
    },

    _onLoaderError: function(loader) {
        this._settingsButton.get_child().destroy();
        this._settingsButton.child = new Gtk.Image ({ icon_size: Gtk.IconSize.MENU,
                                                      icon_name: 'dialog-warning-symbolic',
                                                      halign: Gtk.Align.END,
                                                      visible: true });
    },

    _onLoaderLoading: function(loader) {
        let spinner = new Gtk.Spinner();
        spinner.set_size_request(16, 16);
        spinner.start();
        spinner.show();

        this._settingsButton.get_child().destroy();
        this._settingsButton.child = spinner;
    },

    _onLoaderChanged : function(loader) {
        let weather = loader.getXml();
        let props = loader.getProps();

        if (weather == null || props == null) {
            return;
        }

        this._settingsButton.get_child().destroy();
        this._settingsButton.child = new Gtk.Image ({ icon_size: Gtk.IconSize.MENU,
                                                      icon_name: 'system-run-symbolic',
                                                      halign: Gtk.Align.END,
                                                      visible: true });

        let iconName = this._getWeatherIcon(weather.current_conditions.icon.@data.toString());
        this._icon.icon_name = iconName;

        let useF = (weather.forecast_information.unit_system.@data.toString() != 'SI');
        let temp, tempUnit;

        tempUnit = String.fromCharCode(0x00B0);

        if (useF) {
            temp = weather.current_conditions.temp_f.@data.toString();
            tempUnit += 'F';
        } else {
            temp = weather.current_conditions.temp_c.@data.toString();
            tempUnit += 'C';
        }
        this._degreeLabel.set_markup(temp + ' ' + tempUnit);

        let state;
        if (props['countrycode'] == 'US')
            state = props['statecode'];
        else
            state = props['country'];

        let location = props['city'] + ', ' + state;
        this._cityLabel.set_text(location);

        let comment = weather.current_conditions.condition.@data.toString();
        this._condLabel.set_text(comment);

        let humidity = weather.current_conditions.humidity.@data.toString();
        this._humidityLabel.set_text(humidity);

        let wind = weather.current_conditions.wind_condition.@data.toString();
        this._windLabel.set_text(wind);

        for (let i = 0; i < 4; i++) {
            let forecast = weather.forecast_conditions[i];
            let day;

            if (i == 0)
                day = _("Today");
            else
                day = forecast.day_of_week.@data.toString();

            this._forecastBoxes[i].dayLabel.set_text(day);

            let iconPath = forecast.icon.@data.toString();
            this._forecastBoxes[i].image.icon_name = this._getWeatherIcon(iconPath);

            let minMax = ('%f | %f %s').format(
                forecast.low.@data.toString(),
                forecast.high.@data.toString(), tempUnit);
            this._forecastBoxes[i].tempLabel.set_text(minMax);
        }
    },

    _getWeatherIcon : function(googleIconPath) {
        let elements = googleIconPath.split('/');
        let iconName = elements[elements.length - 1];

        if ((iconName.indexOf('mostly_sunny') != -1) ||
            (iconName.indexOf('mostly_cloudy') != -1) ||
            (iconName.indexOf('partly_cloudy') != -1))
            return 'weather-few-clouds';
        else if ((iconName.indexOf('scatteredthunderstorms') != -1) ||
                 (iconName.indexOf('thunderstorm') != -1) ||
                 (iconName.indexOf('chance_of_storm') != -1))
            return 'weather-storm';
        else if ((iconName.indexOf('scatteredshowers') != -1) ||
                 (iconName.indexOf('drizzle') != -1) ||
                 (iconName.indexOf('chance_of_rain') != -1))
            return 'weather-showers-scattered';
        else if ((iconName.indexOf('overcast') != -1) ||
                 (iconName.indexOf('cloudy') != -1))
            return 'weather-overcast';
        else if (iconName.indexOf('sunny') != -1)
            return 'weather-clear';
        else if (iconName.indexOf('rain') != -1)
            return 'weather-showers';
        else if ((iconName.indexOf('fog') != -1) ||
                 (iconName.indexOf('mist') != -1))
            return 'weather-fog';
        else
            return 'weather-snow';
    }
};
