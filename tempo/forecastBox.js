/*
 * Copyright (c) 2011 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

const Gtk = imports.gi.Gtk;
const _ = imports.gettext.gettext;

function ForecastBox(args) {
    this._init();
}

ForecastBox.prototype = {
    _init : function(args) {
        this._createBox();
    },

    _createBox : function() {
        this.grid = new Gtk.Grid({ orientation: Gtk.Orientation.VERTICAL,
                                   row_spacing: 6 });

        this.dayLabel = new Gtk.Label({ name: 'tempo-forecast-day-label' });
        this.grid.add(this.dayLabel);

        this.image = new Gtk.Image({ pixel_size: 24 });
        this.grid.add(this.image);

        this.tempLabel = new Gtk.Label({ name: 'tempo-forecast-temp-label' });
        this.grid.add(this.tempLabel);
    }
};
