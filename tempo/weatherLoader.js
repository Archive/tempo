/*
 * Copyright (c) 2011 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

const GOOGLE_URL = "http://www.google.com/ig/api?hl=%s&weather=%s";

const Signals = imports.signals;

const Geocode = imports.gi.GeocodeGlib;
const GLib = imports.gi.GLib;
const Json = imports.gi.Json;
const Lang = imports.lang;
const Mainloop = imports.mainloop;
const Soup = imports.gi.Soup;
const Weather = imports.gi.Weather;

function WeatherLoader(location) {
    this._init(location);
}

WeatherLoader.prototype = {
    _init : function(location) {
        this._location = location;
        this._googleXml = null;
        this._props = null;

        this.loadCache();
    },

    setLocation: function(location) {
        if (this._location == location)
            return;

        if (this._location == null ||
            this._location == '')
            return;

        this._props = null;
        this._location = location;
        this._refreshWeather();
    },

    getXml: function() {
        return this._googleXml;
    },

    getProps: function() {
        return this._props;
    },

    loadCache: function(callback) {
        let cachePath = GLib.get_user_cache_dir() + '/tempo/last_cache.xml';

        try {
            let contents = GLib.file_get_contents(cachePath);
            let contentsStr = contents[1].toString();

            if (contents[0]) {
                this._googleXml = new XML(contentsStr);

                let geoCode = new Geocode.Object();
                geoCode.add('location', this._location);

                geoCode.resolve_async(null, Lang.bind(this,
                    function(obj, res) {
                        try {
                            let props = geoCode.resolve_finish(res);
                            this._props = props;

                            this.emit('changed');
                        } catch (e) {
                            log('Unable to resolve weather location ' + this._location + ': ' + e.toString());
                        }
                    }));
            }
        } catch (e) {
            log('Unable to load weather cache: ' + e.toString());
        }
    },

    _refreshWeather: function(callback) {
        this.emit('loading');

        if (!this._props) {
            let geoCode = new Geocode.Object();
            geoCode.add('location', this._location);

            geoCode.resolve_async(null, Lang.bind(this,
                function(obj, res) {
                    try {
                        let props = geoCode.resolve_finish(res);
                        this._props = props;

                        this._getWeather();
                    } catch (e) {
                        log('Unable to resolve weather location ' + this._location + ': ' + e.toString());
                        this.emit('error');
                    }
                }));
        } else {
            this._getWeather();
        }
    },

    _getLanguage : function() {
        let languages = GLib.get_language_names();

        if (languages[0].indexOf("_") != -1)
            return languages[1];

        if (languages[0] == "C")
            return "en";

        return languages[0];
    },

    _getWeather : function() {
        let session = new Soup.SessionAsync();
        let language = this._getLanguage();
        let message = Soup.Message.new("GET", GOOGLE_URL.format(language, this._location));

        session.queue_message(message, Lang.bind(this, this._onResponseReceived));
    },

    _onResponseReceived : function(session, message) {
        let utf8 = Weather.convert_response_to_utf8(message);

        try {
            this._googleXml = new XML(utf8.substr(21)).*;
            this._saveCache();

            this.emit('changed');
        } catch (e) {
            log('Unable to parse the remote XML response: ' + e.toString());
            this.emit('error');
        }
    },

    _saveCache: function() {
        let cachePath = GLib.get_user_cache_dir() + '/tempo';
        GLib.mkdir_with_parents(cachePath, 0700);

        cachePath += '/last_cache.xml';

        try {
            GLib.file_set_contents(cachePath, this._googleXml.toXMLString());
        } catch (e) {
            log('Unable to save cache file: ' + e.toString());
        }
    }
};
Signals.addSignalMethods(WeatherLoader.prototype);
