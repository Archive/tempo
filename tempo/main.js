/*
 * Copyright (c) 2011 Cosimo Cecchi <cosimoc@gnome.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 2 of the License, or (at your
 * option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

const Gettext = imports.gettext;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Gdk = imports.gi.Gdk;
const Gtk = imports.gi.Gtk;
const Path = imports.path;
const _ = imports.gettext.gettext;

const Format = imports.format;
const MainWindow = imports.mainWindow;
const WeatherLoader = imports.weatherLoader;

let settings = null;
let loader = null;

function start() {
    Gettext.bindtextdomain('tempo', Path.LOCALE_DIR);
    Gettext.textdomain('tempo');
    String.prototype.format = Format.format;

    GLib.set_prgname('tempo');
    Gtk.init(null, null);

    let gtkSettings = Gtk.Settings.get_default();
    gtkSettings.gtk_application_prefer_dark_theme = true;

    let provider = new Gtk.CssProvider();
    provider.load_from_path(Path.STYLE_DIR + "gtk-style.css");
    Gtk.StyleContext.add_provider_for_screen(Gdk.Screen.get_default(),
        provider, 600);

    settings = Gio.Settings.new('org.gnome.tempo');
    loader = new WeatherLoader.WeatherLoader(settings.get_string('last-location'));
    let mainWindow = new MainWindow.MainWindow();

    Gtk.main();
}
